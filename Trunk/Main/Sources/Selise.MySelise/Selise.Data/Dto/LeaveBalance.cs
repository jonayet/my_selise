﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selise.Data.Dto
{
    public class LeaveBalance
    {
        public string FullName { get; set; }
        public double RemainingLeave { get; set; }

    }
}
