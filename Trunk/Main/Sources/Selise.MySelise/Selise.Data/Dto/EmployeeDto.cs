﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selise.Data.Dto
{
    public class EmployeeDto
    {
        public int Employee_ID { get; set; }
        public string Employee_FullName { get; set; }
        public Nullable<int> Department_ID { get; set; }
        public Nullable<int> Employee_Position_ID { get; set; }
        public string Employee_Email { get; set; }
        public Nullable<int> Active { get; set; }
        public Nullable<System.DateTime> Start_Date { get; set; }
        public Nullable<System.DateTime> End_Date { get; set; }
        public System.DateTime Date_Of_Birth { get; set; }
        public string Blood_Group { get; set; }
        public string Address_Line_1 { get; set; }
        public string Address_Line_2 { get; set; }
        public string Suburb { get; set; }
        public string Post_Code { get; set; }
        public string Primary_Contact_No { get; set; }
        public string Emergency_Contact_No { get; set; }
        public string Emergency_Contact_Relation { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
    }
}
