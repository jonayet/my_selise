﻿using System.Collections.Generic;
using Selise.Data.DatabaseContext;

namespace Selise.Data.Manager.Contracts
{
   public  interface IDesignationManager
    {
        IEnumerable<Designation> GetAll();
    }
}
