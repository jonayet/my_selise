﻿using Selise.Data.DatabaseContext;

namespace Selise.Data.Manager.Contracts
{
    public interface IEmployeeManager
    {
        void AddNewEmployee(Employee employee);
        Employee GetEmployee(int employeeId);
        void UpdateEmployeeProfile(Employee employee);
    }
}
