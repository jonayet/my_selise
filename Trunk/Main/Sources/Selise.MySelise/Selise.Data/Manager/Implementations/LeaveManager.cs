﻿using System;
using System.Collections.Generic;
using System.Linq;
using Selise.Data.Contracts;
using Selise.Data.DatabaseContext;
using Selise.Data.Dto;
using Selise.Data.Manager.Contracts;

namespace Selise.Data.Manager.Implementations
{
    public class LeaveManager : ILeaveManager
    {
        private readonly IRepository<LeaveApplication> leaveRepository;
        private readonly IRepository<Holiday> holidayRepository;

        public LeaveManager(IRepository<LeaveApplication> tblLeaveRepository, IRepository<Holiday> tblPublicHolidayRepository)
        {
            this.leaveRepository = tblLeaveRepository;
            this.holidayRepository = tblPublicHolidayRepository;
        }

        public LeaveBalance GetLeaveBalanceByUserId(int id)
        {
            double numberOfHolidayInAppliedLeave = 0;
            double actualLeaveTaken = 0;
            //var appliedLeave = _leaveRepository.GetAll().Where(c => c.Employee_ID == id).ToList();
            //var holidays = _holidayRepository.GetAll().Select(c=>c.PublicHolidayDate).ToList();
            //if (appliedLeave.Count > 0)
            //{
            //    numberOfHolidayInAppliedLeave += appliedLeave.Sum(tblLeaf => GetNumberOfHolidaysInLeave(tblLeaf.Leave_Start_Date, tblLeaf.Leave_End_Date, tblLeaf.Is_Half_Date, holidays));
            //    foreach (var dateTime in appliedLeave)
            //    {
            //        if (dateTime.Leave_End_Date.Date > dateTime.Leave_Start_Date.Date)
            //        actualLeaveTaken += ((dateTime.Leave_End_Date - dateTime.Leave_Start_Date).TotalDays + 1);
            //        else
            //        {
            //            if (dateTime.Is_Half_Date == 1)
            //            {
            //                actualLeaveTaken += .5;
            //            }
            //            else
            //            {
            //                actualLeaveTaken++;
            //            }
            //        }
            //    }
            //}


            return new LeaveBalance
            {
                FullName = "Test",
                RemainingLeave = actualLeaveTaken - numberOfHolidayInAppliedLeave
            };
        }

        protected double GetNumberOfHolidaysInLeave(DateTime leaveStartDate, DateTime leaveEndDate, int isHalfDate, List<DateTime> holidays)
        {
            double numberOfHolidayInAppliedLeave = 0;

            foreach (var dateTime in holidays)
            {
                if (leaveStartDate.Date <= dateTime.Date && leaveEndDate.Date >= dateTime.Date)
                {
                    if (isHalfDate == 1)
                    {
                        numberOfHolidayInAppliedLeave -= .5;
                    }
                    else
                    {
                        numberOfHolidayInAppliedLeave--;
                    }
                }                
            }
            return numberOfHolidayInAppliedLeave;
        }        
    }
}
