﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Selise.Data.Contracts;
using Selise.Data.DatabaseContext;
using Selise.Data.Manager.Contracts;

namespace Selise.Data.Manager.Implementations
{
    public class DesignationManager : IDesignationManager
    {
        private readonly IRepository<Designation> designationRepository;

        public DesignationManager(IRepository<Designation> designationRepository)
        {
            this.designationRepository = designationRepository;
        }

        public IEnumerable<Designation> GetAll()
        {
            return this.designationRepository.GetAll();
        }
    }
}
