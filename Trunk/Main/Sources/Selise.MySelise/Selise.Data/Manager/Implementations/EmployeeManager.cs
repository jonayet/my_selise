﻿using System.Linq;
using Selise.Data.BLL;
using Selise.Data.Contracts;
using Selise.Data.DatabaseContext;
using Selise.Data.Dto;
using Selise.Data.Manager.Contracts;

namespace Selise.Data.Manager.Implementations
{
    public class EmployeeManager : IEmployeeManager
    {
        private readonly IRepository<Employee> employeerRepository;
        private readonly IUnitOfWork unitOfWork;

        public EmployeeManager(IRepository<Employee> employeeRepository, IUnitOfWork unitOfWork)
        {
            this.employeerRepository = employeeRepository;
            this.unitOfWork = unitOfWork;
        }

        public void AddNewEmployee(Employee employee)
        {            
            this.employeerRepository.Add(employee);
            this.unitOfWork.Commit();
        }

        public Employee GetEmployee(int employeeId)
        {
            var employee = this.employeerRepository.FirstOrDefault(e => e.Id == employeeId);
            return employee;
        }

        public void UpdateEmployeeProfile(Employee employee)
        {
            this.employeerRepository.Attach(employee);
            this.unitOfWork.Commit();
        }
    }
}
