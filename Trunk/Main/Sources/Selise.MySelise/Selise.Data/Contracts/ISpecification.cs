﻿using System;
using System.Linq.Expressions;

namespace Selise.Data.Contracts
{
    public interface ISpecification<TE>
    {
        /// <summary>
        /// Select/Where Expression
        /// </summary>
        Expression<Func<TE, bool>> EvalPredicate { get; }
        /// <summary>
        /// Function to evaluate where Expression
        /// </summary>
        Func<TE, bool> EvalFunc { get; }
    }
}