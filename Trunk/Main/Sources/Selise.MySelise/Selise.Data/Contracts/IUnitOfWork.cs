﻿namespace Selise.Data.Contracts
{
    public interface IUnitOfWork
    {
        void Commit();
        void Dispose();
    }
}