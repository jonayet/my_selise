﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Selise.Data.Contracts
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Queryable();        
        IEnumerable<T> GetAll();
        IEnumerable<T> Where(Expression<Func<T, bool>> where);
        T Single(Expression<Func<T, bool>> where);
        T First(Expression<Func<T, bool>> where);
        T FirstOrDefault(Expression<Func<T, bool>> where);
        
        bool Delete(T entity);
        bool Add(T entity);
        bool Attach(T entity);
    }
}