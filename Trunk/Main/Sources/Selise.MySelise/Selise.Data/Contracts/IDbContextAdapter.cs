﻿using System;
using System.Data.Entity;

namespace Selise.Data.Contracts
{
    public interface IDbContextAdapter : IDisposable
    {
        IDbSet<T> CreateDbSet<T>() where T : class;
        void SaveChanges();
    }
}