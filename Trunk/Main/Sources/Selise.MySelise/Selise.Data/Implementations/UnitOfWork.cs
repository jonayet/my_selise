﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using Selise.Data.Contracts;

namespace Selise.Data.Implementations
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly IDbContextAdapter _dbContextAdapter;

        public UnitOfWork(IDbContextAdapter dbContextAdapter)
        {
            _dbContextAdapter = dbContextAdapter;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_dbContextAdapter != null)
            {
                _dbContextAdapter.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        #endregion

        #region IUnitOfWork Members

        public void Commit()
        {
            //_dbContextAdapter.SaveChanges();

            try
            {
                // Your code...
                // Could also be before try if you know the exception occurs in SaveChanges

                _dbContextAdapter.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:",
                        DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    outputLines.AddRange(eve.ValidationErrors.Select(ve => string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage)));
                }
                System.IO.File.AppendAllLines(@"c:\temp\errors.txt", outputLines);
                throw;                
            }
        }

        #endregion
    }
}