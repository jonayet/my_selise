﻿using System;
using System.Linq.Expressions;
using Selise.Data.Contracts;
using Selise.Data.Helper;

namespace Selise.Data.Implementations
{
    public class Specification<TE> : ISpecification<TE>
    {
        #region Private Members

        private Func<TE, bool> _evalFunc = null;
        private Expression<Func<TE, bool>> _evalPredicate;

        #endregion

        #region Virtual Accessors

        public virtual bool Matches(TE entity)
        {
            return _evalPredicate.Compile().Invoke(entity);
        }

        public virtual Expression<Func<TE, bool>> EvalPredicate
        {
            get { return _evalPredicate; }
        }

        public virtual Func<TE, bool> EvalFunc
        {
            get { return _evalPredicate != null ? _evalPredicate.Compile() : null; }
        }

        #endregion

        #region Constructors

        public Specification(Expression<Func<TE, bool>> predicate)
        {
            _evalPredicate = predicate;
        }

        private Specification() { }

        #endregion

        #region Private Nested Classes

        private class AndSpecification : Specification<TE>
        {
            private readonly ISpecification<TE> _left;
            private readonly ISpecification<TE> _right;
            public AndSpecification(ISpecification<TE> left, ISpecification<TE> right)
            {
                this._left = left;
                this._right = right;

                this._evalFunc =
                    (Func<TE, bool>)Delegate.Combine
                    (left.EvalPredicate.Compile(),
                    right.EvalPredicate.Compile());                
                _evalPredicate = left.EvalPredicate.And( right.EvalPredicate);
            }
            public override bool Matches(TE entity)
            {
                return EvalPredicate.Compile().Invoke(entity);
            }
        }

        private class OrSpecification : Specification<TE>
        {
            private readonly ISpecification<TE> _left;
            private readonly ISpecification<TE> _right;
            public OrSpecification(ISpecification<TE> left, ISpecification<TE> right)
            {
                this._left = left;
                this._right = right;

                this._evalFunc =
                    (Func<TE, bool>)Delegate.Combine
                    (left.EvalPredicate.Compile(),
                    right.EvalPredicate.Compile());

                _evalPredicate = left.EvalPredicate.Or(right.EvalPredicate);
            }
            public override bool Matches(TE entity)
            {
                return EvalPredicate.Compile().Invoke(entity);
            }
        }

        private class NotSpecification : Specification<TE>
        {
            private readonly ISpecification<TE> _left;
            private readonly ISpecification<TE> _right;
            public NotSpecification(ISpecification<TE> left)
            {
                this._left = left;
                

                this._evalFunc =
                    (Func<TE, bool>)Delegate.Combine
                    (left.EvalPredicate.Compile());

                _evalPredicate = left.EvalPredicate.Not();
            }
            public override bool Matches(TE entity)
            {
                return EvalPredicate.Compile().Invoke(entity);
            }
        }


//        public class NotSpecification<TEntity> : CompositeSpecification<TEntity>
//{
//    public NotSpecification(Specification<TEntity> leftSide, Specification<TEntity> rightSide)
//        : base(leftSide, rightSide)
//    {
//    }
 
//    public override bool IsSatisfiedBy(TEntity entity)
//    {
//        return _leftSide.IsSatisfiedBy(entity) && !_rightSide.IsSatisfiedBy(entity);
//    }
//}

        #endregion

        #region Operator Overloads

        public static Specification<TE> operator &(Specification<TE> left, ISpecification<TE> right)
        {
            return new AndSpecification(left, right);
        }

        public static Specification<TE> operator |(Specification<TE> left, ISpecification<TE> right)
        {
            return new OrSpecification(left, right);
        }

        public static Specification<TE> operator !(Specification<TE> left)
        {
            return new NotSpecification(left);
        }

        #endregion

    } 
}