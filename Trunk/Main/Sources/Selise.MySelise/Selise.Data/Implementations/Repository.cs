﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Selise.Data.Contracts;

namespace Selise.Data.Implementations
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly IDbSet<T> DbSet;

        public Repository(IDbContextAdapter objectContextAdapter)
        {
            DbSet = objectContextAdapter.CreateDbSet<T>();
        }

       #region IRepository<T> Members

        public IQueryable<T> Queryable()
        {
            return DbSet;
        }       

        public IEnumerable<T> GetAll()
        {
            return DbSet;
        }

        public IEnumerable<T> Where(Expression<Func<T, bool>> where)
        {
            return DbSet.Where(where);
        }

        public T Single(Expression<Func<T, bool>> where)
        {
            return DbSet.Single(where);
        }

        public T First(Expression<Func<T, bool>> where)
        {
            return DbSet.First(where);
        }

        public T FirstOrDefault(Expression<Func<T, bool>> where)
        {
            return DbSet.FirstOrDefault(where);
        }

        public bool Delete(T entity)
        {
            DbSet.Remove(entity);
            return true;
        }

        public bool Add(T entity)
        {
            DbSet.Add(entity);
            return true;
        }

        public bool Attach(T entity)
        {
            DbSet.Attach(entity);
            return true;
        }

        #endregion
    }

    //public static class RepositoryExtension
    //{
    //    public static IQueryable<T> IncludeMultiple<T>(this IQueryable<T> query, params Expression<Func<T, object>>[] includes)
    //        where T : class
    //    {
    //        if (includes != null)
    //        {
    //            query = includes.Aggregate(query,
    //                                       (current, include) => current.Include<T, object>(include));
    //        }

    //        return query;
    //    }
    //}
}