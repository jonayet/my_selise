﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using Selise.Data.Contracts;

namespace Selise.Data.Implementations
{
    public class DbContextAdapter : IDbContextAdapter
    {
        private readonly DbContext _context;

        public DbContextAdapter(DbContext context)
        {
            _context = context;
        }

        #region IDbContextAdapter Members

        public void Dispose()
        {
            _context.Dispose();
        }

        public IDbSet<T> CreateDbSet<T>() where T : class
        {            
            return _context.Set<T>();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        #endregion
    }
}