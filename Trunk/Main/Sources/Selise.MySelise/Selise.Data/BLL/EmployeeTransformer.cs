﻿using Selise.Data.DatabaseContext;
using Selise.Data.Dto;

namespace Selise.Data.BLL
{
    public class EmployeeTransformer
    {
        public EmployeeDto GetEmployeeDto(Employee employee)
        {
            if (employee == null) { return null; }
            var employeeDto = new EmployeeDto
            {
                //Active = employee.Active,
                //Address_Line_1 = employee.Address_Line_1,
                //Address_Line_2 = employee.Address_Line_2,
                //Blood_Group = employee.Blood_Group,
                //CreatedBy = employee.CreatedBy,
                //CreatedOn = employee.CreatedOn,
                //Date_Of_Birth = employee.Date_Of_Birth,
                //Department_ID = employee.Department_ID,
                //Emergency_Contact_No = employee.Emergency_Contact_No,
                //Emergency_Contact_Relation = employee.Emergency_Contact_Relation,
                //Employee_Email = employee.Employee_Email,
                //Employee_FullName = employee.Employee_FullName,
                //Employee_ID = employee.Employee_ID,
                //Employee_Position_ID = employee.Employee_Position_ID,
                //End_Date = employee.End_Date,
                //Post_Code = employee.Post_Code,
                //Primary_Contact_No = employee.Primary_Contact_No,
                //Start_Date = employee.Start_Date,
                //Suburb = employee.Suburb,
            };
            return employeeDto;
        }

        public Employee RevertEmployeeInfo(EmployeeDto employeeDto)
        {
            if (employeeDto == null) { return null; }
            var employee = new Employee
            {
                //Active = employeeDto.Active,
                //Address_Line_1 = employeeDto.Address_Line_1,
                //Address_Line_2 = employeeDto.Address_Line_2,
                //Blood_Group = employeeDto.Blood_Group,
                //CreatedBy = employeeDto.CreatedBy,
                //CreatedOn = employeeDto.CreatedOn,
                //Date_Of_Birth = employeeDto.Date_Of_Birth,
                //Department_ID = employeeDto.Department_ID,
                //Emergency_Contact_No = employeeDto.Emergency_Contact_No,
                //Emergency_Contact_Relation = employeeDto.Emergency_Contact_Relation,
                //Employee_Email = employeeDto.Employee_Email,
                //Employee_FullName = employeeDto.Employee_FullName,
                //Employee_ID = employeeDto.Employee_ID,
                //Employee_Position_ID = employeeDto.Employee_Position_ID,
                //End_Date = employeeDto.End_Date,
                //Post_Code = employeeDto.Post_Code,
                //Primary_Contact_No = employeeDto.Primary_Contact_No,
                //Start_Date = employeeDto.Start_Date,
                //Suburb = employeeDto.Suburb,
            };
            return employee;
        }
    }
}
