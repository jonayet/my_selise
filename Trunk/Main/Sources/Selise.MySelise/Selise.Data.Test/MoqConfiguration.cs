﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FizzWare.NBuilder;
using Moq;
using Selise.Data.DatabaseContext;

namespace Selise.Data.Test
{
    public interface IPilsnerContext
    {
        DbSet<T> Set<T>() where T : class;
    }

    public class TestDbContext : DbContext, IPilsnerContext, IDisposable
    {
        
        public TestDbContext() 
        { 
            this.Configuration.ProxyCreationEnabled = false; 
        }  
       // public virtual DbSet<tblPositionTitle> tblPositionTitles { get; set; }


    }

    public class FakeDbSet<T> : Mock<DbSet<T>> where T : class
    {
        public void SetData(IEnumerable<T> data)
        {
            var mockDataQueryable = data.AsQueryable();

            this.As<IQueryable<T>>().Setup(x => x.Provider).Returns(mockDataQueryable.Provider);
            this.As<IQueryable<T>>().Setup(x => x.Expression).Returns(mockDataQueryable.Expression);
            this.As<IQueryable<T>>().Setup(x => x.ElementType).Returns(mockDataQueryable.ElementType);
            this.As<IQueryable<T>>().Setup(x => x.GetEnumerator()).Returns(mockDataQueryable.GetEnumerator());
        }
    }


    public class GenericMoqConfiguration<T> where T : class
    {

        private readonly Mock<TestDbContext> _mock;
        private readonly TestDbContext _context;
        public GenericMoqConfiguration()
        {
            _mock = new Mock<TestDbContext>();
            SetPositionTitle();

            _context = _mock.Object;
        }



        private void SetPositionTitle()
        {
            var tblPositionTitles = Builder<T>.CreateListOfSize(10).Build().AsQueryable();
            var setPositionTitle = new FakeDbSet<T>();
            setPositionTitle.SetData(tblPositionTitles);
            _mock.Setup(c => c.Set<T>()).Returns(setPositionTitle.Object);
        }

        public TestDbContext GetContext()
        {
            return this._context;
        }
    }

    public class MoqConfiguration
    {

        private readonly Mock<TestDbContext> _mock;
        private readonly TestDbContext _context ;
        public MoqConfiguration()
        {
            _mock = new Mock<TestDbContext>();
            SetDbSets();

            _context = _mock.Object;
        }



        private void SetDbSets()
        {
            var tblPositionTitles = Builder<Designation>.CreateListOfSize(10).Build().AsQueryable();
            var setPositionTitle = new FakeDbSet<Designation>();
            setPositionTitle.SetData(tblPositionTitles);
            _mock.Setup(c => c.Set<Designation>()).Returns(setPositionTitle.Object);

            var tbltblLeave = Builder<LeaveApplication>.CreateListOfSize(10).Build().AsQueryable();
            var settblLeave = new FakeDbSet<LeaveApplication>();
            settblLeave.SetData(tbltblLeave);
            _mock.Setup(c => c.Set<LeaveApplication>()).Returns(settblLeave.Object);

        }

        public TestDbContext GetContext()
        {
            var ctx = _context;
            return this._context;
        }
    }
}
