﻿using System.Data.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzWare.NBuilder;
using Selise.Data.Contracts;
using Selise.Data.DatabaseContext;
using Selise.Data.Implementations;
using Selise.Data.Manager.Contracts;
using Selise.Data.Manager.Implementations;

namespace Selise.Data.Test
{
    [TestClass]
    public class LeaveManagerUnitTest
    {
        private DbContext dbContext;
        private IDbContextAdapter contextAdapter;

        private ILeaveManager leaveManager;
        private IRepository<LeaveApplication> leaveApplicationRepository;
        private IRepository<Holiday> holidayRepository;

        [TestInitialize]
        public void Init()
        {
            this.dbContext = new mySeliseEntities();
            this.contextAdapter = new DbContextAdapter(this.dbContext);
            leaveApplicationRepository = new Repository<LeaveApplication>(this.contextAdapter);
            holidayRepository = new Repository<Holiday>(this.contextAdapter);
            this.leaveManager = new LeaveManager(leaveApplicationRepository, holidayRepository);
        }

        [TestMethod]
        public void TestMethod1()
        {
            var model = this.leaveManager.GetLeaveBalanceByUserId(3);
            Assert.IsNotNull(model);
        }
    }
}
