﻿using FizzWare.NBuilder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Selise.Data.Contracts;
using Selise.Data.DatabaseContext;
using Selise.Data.Dto;
using Selise.Data.Implementations;
using Selise.Data.Manager.Contracts;
using Selise.Data.Manager.Implementations;
using System.Data.Entity;

namespace Selise.Data.Test
{
    [TestClass]
    public class EmployeeManagerUnitTest
    {
        private DbContext dbContext;
        private IDbContextAdapter contextAdapter;
        private IUnitOfWork unitOfWork;
        private IEmployeeManager employeeManager;
        private IRepository<Employee> employeerRepository;

        [TestInitialize]
        public void Init()
        {
            this.dbContext = new mySeliseEntities();
            this.contextAdapter = new DbContextAdapter(this.dbContext);
            this.unitOfWork = new UnitOfWork(this.contextAdapter);
            this.employeerRepository = new Repository<Employee>(this.contextAdapter);
            this.employeeManager = new EmployeeManager(this.employeerRepository, this.unitOfWork);
        }

        [TestMethod]
        public void GetEmployeeByIdTest()
        {
            var employee = this.employeeManager.GetEmployee(1);
            Assert.IsNotNull(employee);
        }

        [TestMethod]
        public void AddNewEmployeeTest()
        {
            var employee = Builder<Employee>.CreateNew().Build();
            this.employeeManager.AddNewEmployee(employee);
            this.unitOfWork.Commit();
            Assert.IsNotNull(employee);
        }

        [TestMethod]
        public void UpdateEmployeeInfoTest()
        {
            var employee = Builder<Employee>.CreateNew().Build();
            this.employeeManager.UpdateEmployeeProfile(employee);
            Assert.IsNotNull(employee);
        }
    }
}
