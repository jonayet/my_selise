﻿using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Selise.Data.Contracts;
using Selise.Data.DatabaseContext;
using Selise.Data.Implementations;
using Selise.Data.Manager.Contracts;
using Selise.Data.Manager.Implementations;

namespace Selise.Data.Test
{
    [TestClass]
    public class PositionTitleUnitTest
    {

        private IRepository<Designation> designationRepository;
        private DbContext dbContext;
        private IDbContextAdapter contextAdapter;
      //  IUnitOfWork _unitOfWork;
        private IDesignationManager designationManager;

        [TestInitialize]
        public void Init()
         {
             var configuration = new MoqConfiguration();

            this.dbContext = configuration.GetContext();

             this.contextAdapter = new DbContextAdapter(this.dbContext);
        //     _unitOfWork = new UnitOfWork(_contextAdapter);

             designationRepository = new Repository<Designation>(this.contextAdapter);
             this.designationManager = new DesignationManager(designationRepository);
         }

        [TestMethod]
        public void TestMethod1()
        {
            designationRepository.Add(new Designation
            {
                Title = "New one",
            });

            this.dbContext.SaveChanges();

            var lists = this.designationManager.GetAll();
            var count = lists.Count();
            Assert.IsNotNull(lists);
        }
    }
}
