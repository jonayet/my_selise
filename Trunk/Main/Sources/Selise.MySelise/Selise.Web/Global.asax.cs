﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Selise.Web.Windsor;

namespace Selise.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private readonly WindsorContainer _container;

        public MvcApplication()
        {
            this._container = new WindsorContainer();
        }

        protected void Application_Start()
        {
           
           

            AreaRegistration.RegisterAllAreas();

            InstallDependencies();
            this.RegisterDependencyResolver();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void InstallDependencies()
        {
            this._container.Install(FromAssembly.This());
        }

        private void RegisterDependencyResolver()
        {
            GlobalConfiguration.Configuration.DependencyResolver = new WindsorDependencyResolver(this._container.Kernel);
        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (this._container != null)
                this._container.Dispose();
        }
    }
}
