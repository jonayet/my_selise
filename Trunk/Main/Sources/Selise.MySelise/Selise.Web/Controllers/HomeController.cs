﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Selise.Data.Manager.Contracts;

namespace Selise.Web.Controllers
{
    public class HomeController : Controller
    {
        public IDesignationManager designationManager;
        private readonly ILeaveManager _leaveManager;
        private IEmployeeManager _employeeManager;

        public HomeController(IDesignationManager designationManager, ILeaveManager leaveManager, IEmployeeManager employeeManager)
        {
            _employeeManager = employeeManager;
            this.designationManager = designationManager;
            _leaveManager = leaveManager;
        }

        public ActionResult Index()
        {
            var employeeDto = _employeeManager.GetEmployee(1);
            var data = this.designationManager.GetAll();
            return View();
        }

        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        [Authorize]
        public ActionResult Secure()
        {
            ViewBag.Message = "Secure page.";
            return View();
        }
    }
}