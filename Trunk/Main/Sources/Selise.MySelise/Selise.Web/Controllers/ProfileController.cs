﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Selise.Data.DatabaseContext;
using System.Security.Claims;

namespace Selise.Web.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly mySeliseEntities db;
        private readonly string claimedEmailAddress;
        private readonly Employee employee;

        public ProfileController()
        {
            db = new mySeliseEntities();
            var emailClaim = ClaimsPrincipal.Current.Claims.FirstOrDefault(c => c.Type.EndsWith("/emailaddress"));
            if (emailClaim == null) { return; }

            claimedEmailAddress = emailClaim.Value;
            if (string.IsNullOrWhiteSpace(claimedEmailAddress)) { return; }

            employee = db.Employees.AsNoTracking().FirstOrDefault(e => e.OfficialEmail == claimedEmailAddress);
        }

        public ActionResult ViewProfile()
        {
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        public ActionResult Update()
        {
            if (employee == null)
            {
                return HttpNotFound();
            }

            ViewBag.BloodGroupId = new SelectList(db.BloodGroups, "Id", "Title", employee.BloodGroupId);
            ViewBag.CountryId = new SelectList(db.Countries, "Id", "Name", employee.CountryId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title", employee.DepartmentId);
            ViewBag.DesignationId = new SelectList(db.Designations, "Id", "Title", employee.DesignationId);
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "Name", employee.DistrictId);
            ViewBag.CreatedBy = new SelectList(db.Employees, "Id", "FullName", employee.CreatedBy);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Title", employee.GenderId);
            ViewBag.JobLocationId = new SelectList(db.JobLocations, "Id", "Location", employee.JobLocationId);
            ViewBag.ModifiedBy = new SelectList(db.Employees, "Id", "FullName", employee.ModifiedBy);
            ViewBag.PoliceStationId = new SelectList(db.PoliceStations, "Id", "Name", employee.PoliceStationId);
            ViewBag.PostOfficeId = new SelectList(db.PostOffices, "Id", "Name", employee.PostOfficeId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Title", employee.RoleId);
            return View(employee);
        }

        public ActionResult Edit()
        {
            if (employee == null)
            {
                return HttpNotFound();
            }

            ViewBag.BloodGroupId = new SelectList(db.BloodGroups, "Id", "Title", employee.BloodGroupId);
            ViewBag.CountryId = new SelectList(db.Countries, "Id", "Name", employee.CountryId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title", employee.DepartmentId);
            ViewBag.DesignationId = new SelectList(db.Designations, "Id", "Title", employee.DesignationId);
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "Name", employee.DistrictId);
            ViewBag.CreatedBy = new SelectList(db.Employees, "Id", "FullName", employee.CreatedBy);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Title", employee.GenderId);
            ViewBag.JobLocationId = new SelectList(db.JobLocations, "Id", "Location", employee.JobLocationId);
            ViewBag.ModifiedBy = new SelectList(db.Employees, "Id", "FullName", employee.ModifiedBy);
            ViewBag.PoliceStationId = new SelectList(db.PoliceStations, "Id", "Name", employee.PoliceStationId);
            ViewBag.PostOfficeId = new SelectList(db.PostOffices, "Id", "Name", employee.PostOfficeId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Title", employee.RoleId);
            return View(employee);
        }

        public ActionResult ListAllProfile()
        {
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employee
        public ActionResult Index()
        {
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View("Details", employee);
        }

        // GET: Employee/Details/5
        public ActionResult Details(int? id)
        {
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            ViewBag.BloodGroupId = new SelectList(db.BloodGroups, "Id", "Title");
            ViewBag.CountryId = new SelectList(db.Countries, "Id", "Name");
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title");
            ViewBag.DesignationId = new SelectList(db.Designations, "Id", "Title");
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "Name");
            ViewBag.CreatedBy = new SelectList(db.Employees, "Id", "FullName");
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Title");
            ViewBag.JobLocationId = new SelectList(db.JobLocations, "Id", "Location");
            ViewBag.ModifiedBy = new SelectList(db.Employees, "Id", "FullName");
            ViewBag.PoliceStationId = new SelectList(db.PoliceStations, "Id", "Name");
            ViewBag.PostOfficeId = new SelectList(db.PostOffices, "Id", "Name");
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Title");
            return View();
        }

        // POST: Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FullName,CellNumber,Photo,GenderId,IsMarried,DateOfBirth,BloodGroupId,AddressLine1,AddressLine2,PoliceStationId,PostOfficeId,DistrictId,CountryId,EmergencyContactName,EmergencyContactCellNumber,EmergencyContactRelationship,AcademicTitle,PostGraduationInstitute,GraduationInstitute,NationalId,TinId,OfficialEmail,AlternativeEmail,FacebookProfile,LinkedinProfile,GoogleplusProfile,SkypeId,DepartmentId,DesignationId,JobLocationId,RoleId,IsActive,JoiningDate,LeavingDate,CreatedOn,CreatedBy,ModifiedOn,ModifiedBy")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BloodGroupId = new SelectList(db.BloodGroups, "Id", "Title", employee.BloodGroupId);
            ViewBag.CountryId = new SelectList(db.Countries, "Id", "Name", employee.CountryId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title", employee.DepartmentId);
            ViewBag.DesignationId = new SelectList(db.Designations, "Id", "Title", employee.DesignationId);
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "Name", employee.DistrictId);
            ViewBag.CreatedBy = new SelectList(db.Employees, "Id", "FullName", employee.CreatedBy);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Title", employee.GenderId);
            ViewBag.JobLocationId = new SelectList(db.JobLocations, "Id", "Location", employee.JobLocationId);
            ViewBag.ModifiedBy = new SelectList(db.Employees, "Id", "FullName", employee.ModifiedBy);
            ViewBag.PoliceStationId = new SelectList(db.PoliceStations, "Id", "Name", employee.PoliceStationId);
            ViewBag.PostOfficeId = new SelectList(db.PostOffices, "Id", "Name", employee.PostOfficeId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Title", employee.RoleId);
            return View(employee);
        }

        // POST: Employee/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FullName,CellNumber,Photo,GenderId,IsMarried,DateOfBirth,BloodGroupId,AddressLine1,AddressLine2,PoliceStationId,PostOfficeId,DistrictId,CountryId,EmergencyContactName,EmergencyContactCellNumber,EmergencyContactRelationship,AcademicTitle,PostGraduationInstitute,GraduationInstitute,NationalId,TinId,OfficialEmail,AlternativeEmail,FacebookProfile,LinkedinProfile,GoogleplusProfile,SkypeId")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee.OfficialEmail = this.employee.OfficialEmail;
                employee.IsActive = this.employee.IsActive;
                employee.DepartmentId = this.employee.DepartmentId;
                employee.DesignationId = this.employee.DesignationId;
                employee.CreatedBy = this.employee.CreatedBy;
                employee.JobLocationId = this.employee.JobLocationId;
                employee.ModifiedOn = DateTime.Now;
                employee.ModifiedBy = this.employee.Id;
                employee.RoleId = this.employee.RoleId;
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ViewProfile");
            }
            ViewBag.BloodGroupId = new SelectList(db.BloodGroups, "Id", "Title", employee.BloodGroupId);
            ViewBag.CountryId = new SelectList(db.Countries, "Id", "Name", employee.CountryId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Title", employee.DepartmentId);
            ViewBag.DesignationId = new SelectList(db.Designations, "Id", "Title", employee.DesignationId);
            ViewBag.DistrictId = new SelectList(db.Districts, "Id", "Name", employee.DistrictId);
            ViewBag.CreatedBy = new SelectList(db.Employees, "Id", "FullName", employee.CreatedBy);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Title", employee.GenderId);
            ViewBag.JobLocationId = new SelectList(db.JobLocations, "Id", "Location", employee.JobLocationId);
            ViewBag.ModifiedBy = new SelectList(db.Employees, "Id", "FullName", employee.ModifiedBy);
            ViewBag.PoliceStationId = new SelectList(db.PoliceStations, "Id", "Name", employee.PoliceStationId);
            ViewBag.PostOfficeId = new SelectList(db.PostOffices, "Id", "Name", employee.PostOfficeId);
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Title", employee.RoleId);
            return View(employee);
        }

        // GET: Employee/Delete/5
        public ActionResult Delete()
        {
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed()
        {
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
