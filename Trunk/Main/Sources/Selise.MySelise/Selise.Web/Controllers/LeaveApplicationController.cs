﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Selise.Data.DatabaseContext;

namespace Selise.Web.Controllers
{
    using System.Security.Claims;

    [Authorize]
    public class LeaveApplicationController : Controller
    {
        private readonly mySeliseEntities db;
        private readonly string claimedEmailAddress;
        private readonly Employee employee;

        public LeaveApplicationController()
        {
            db = new mySeliseEntities();

            var emailClaim = ClaimsPrincipal.Current.Claims.FirstOrDefault(c => c.Type.EndsWith("/emailaddress"));
            if (emailClaim == null) { return; }

            claimedEmailAddress = emailClaim.Value;
            if ( string.IsNullOrWhiteSpace(claimedEmailAddress)) { return; }

            employee = db.Employees.FirstOrDefault(e => e.OfficialEmail == claimedEmailAddress);
        }

        public ActionResult ListAllApplication()
        {
            var leaveApplications = db.LeaveApplications.Include(l => l.ApprovedByEmployee).Include(l => l.Employee).Where(l => l.EmployeeId == employee.Id);
            return View(leaveApplications.ToList());
        }

        // GET: LeaveApplication
        public ActionResult Index()
        {
            var leaveApplications = db.LeaveApplications.Include(l => l.ApprovedByEmployee).Include(l => l.Employee);
            return View(leaveApplications.ToList());
        }

        // GET: LeaveApplication/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeaveApplication leaveApplication = db.LeaveApplications.Find(id);
            if (leaveApplication == null)
            {
                return HttpNotFound();
            }
            return View(leaveApplication);
        }

        // GET: LeaveApplication/Create
        public ActionResult Create()
        {
            ViewBag.ApprovedBy = new SelectList(db.Employees, "Id", "FullName");
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FullName");
            return View();
        }

        // POST: LeaveApplication/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,EmployeeId,ApplicationDate,ApplicationType,StartFrom,EndTo,DaysApplied,DaysApproved,NoOfHalfDayLeave,IsApporved,Comment,ApprovedBy,ApprovedOn")] LeaveApplication leaveApplication)
        {
            if (ModelState.IsValid)
            {
                db.LeaveApplications.Add(leaveApplication);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ApprovedBy = new SelectList(db.Employees, "Id", "FullName", leaveApplication.ApprovedBy);
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FullName", leaveApplication.EmployeeId);
            return View(leaveApplication);
        }

        // GET: LeaveApplication/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeaveApplication leaveApplication = db.LeaveApplications.Find(id);
            if (leaveApplication == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApprovedBy = new SelectList(db.Employees, "Id", "FullName", leaveApplication.ApprovedBy);
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FullName", leaveApplication.EmployeeId);
            return View(leaveApplication);
        }

        // POST: LeaveApplication/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,EmployeeId,ApplicationDate,ApplicationType,StartFrom,EndTo,DaysApplied,DaysApproved,NoOfHalfDayLeave,IsApporved,Comment,ApprovedBy,ApprovedOn")] LeaveApplication leaveApplication)
        {
            if (ModelState.IsValid)
            {
                db.Entry(leaveApplication).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ApprovedBy = new SelectList(db.Employees, "Id", "FullName", leaveApplication.ApprovedBy);
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "FullName", leaveApplication.EmployeeId);
            return View(leaveApplication);
        }

        // GET: LeaveApplication/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeaveApplication leaveApplication = db.LeaveApplications.Find(id);
            if (leaveApplication == null)
            {
                return HttpNotFound();
            }
            return View(leaveApplication);
        }

        // POST: LeaveApplication/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LeaveApplication leaveApplication = db.LeaveApplications.Find(id);
            db.LeaveApplications.Remove(leaveApplication);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
