﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;
using Castle.MicroKernel;
using Castle.MicroKernel.Lifestyle;
using IDependencyResolver = System.Web.Http.Dependencies.IDependencyResolver;

namespace Selise.Web.Windsor
{
    public class WindsorDependencyScope : IDependencyScope
    {
        private readonly IKernel _container;

        private readonly IDependencyResolver _resolver;

        private readonly IDisposable _scope;

        public WindsorDependencyScope(IKernel container)
        {
            this._container = container;
            this._scope = container.BeginScope();
        }

        public object GetService(Type serviceType)
        {
            return this._container.HasComponent(serviceType) ? this._container.Resolve(serviceType) : null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this._container.ResolveAll(serviceType).Cast<object>();
        }

        public void Dispose()
        {
            this._scope.Dispose();
        }
    }
}