﻿using Castle.Core;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Selise.Data.Manager.Contracts;
using Selise.Data.Manager.Implementations;



namespace Selise.Web.Windsor.Installers
{
    public class ServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //container.AddFacility<LoggingFacility>(f => f.UseLog4Net());
            container.AddFacility<EntityFrameworkFacility>();

            container.Register(
                Component.For<IDesignationManager>().ImplementedBy<DesignationManager>().LifeStyle.Is(LifestyleType.PerWebRequest),
                Component.For<ILeaveManager>().ImplementedBy<LeaveManager>().LifeStyle.Is(LifestyleType.PerWebRequest),
                Component.For<IEmployeeManager>().ImplementedBy<EmployeeManager>().LifeStyle.Is(LifestyleType.PerWebRequest)
            );

            //container.Register(Component.For<ILanguageResource>().ImplementedBy<LanguageResource>());            
            //container.Register(
            //    Component.For<IImageFileHandler>().ImplementedBy<ImageFileHandler>(),
                
            //    Component.For<IMultimediaManager>().ImplementedBy<MultimediaManager>().LifeStyle.Is(LifestyleType.PerWebRequest),
            //    Component.For<IFilterKeyManager>().ImplementedBy<FilterKeyManager>().LifeStyle.Is(LifestyleType.PerWebRequest),
            //    Component.For<IDataModelManager>().ImplementedBy<DataModelManager>().LifeStyle.Is(LifestyleType.PerWebRequest),
            //    Component.For<IPlayListManager>().ImplementedBy<PlayListManager>().LifeStyle.Is(LifestyleType.PerWebRequest),
            //    Component.For<IFriendManager>().ImplementedBy<FriendManager>().LifeStyle.Is(LifestyleType.PerWebRequest),
            //    Component.For<IEmailHandler>().ImplementedBy<EmailHandler>().LifeStyle.Is(LifestyleType.PerWebRequest),
            //    Component.For<IViewedMultimediaManager>().ImplementedBy<ViewedMultimediaManager>().LifeStyle.Is(LifestyleType.PerWebRequest)
            // );
        }
    }
}