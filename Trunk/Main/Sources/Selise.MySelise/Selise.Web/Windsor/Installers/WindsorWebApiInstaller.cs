﻿using System.Web.Http;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Selise.Web.Windsor.Installers
{
    public class WindsorWebApiInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {            
            container.Register(AllTypes.FromThisAssembly().BasedOn<ApiController>().LifestyleScoped());
            container.Register(AllTypes.FromThisAssembly().BasedOn<Controller>().LifestyleScoped());   
        }
    }
}