﻿using System.Data.Entity;
using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using Selise.Data.Contracts;
using Selise.Data.DatabaseContext;
using Selise.Data.Implementations;

namespace Selise.Web.Windsor.Installers
{
    public class EntityFrameworkFacility : AbstractFacility
    {
        protected override void Init()
        {
            Kernel.Register(
                Component.For<DbContext>().ImplementedBy<mySeliseEntities>().LifestylePerWebRequest(),
                Component.For<IDbContextAdapter>().ImplementedBy<DbContextAdapter>().LifestylePerWebRequest(),
                Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestylePerWebRequest(),
                Component.For(typeof (IRepository<>)).ImplementedBy(typeof (Repository<>)).LifestylePerWebRequest());
        }
    }
}