﻿namespace Selise.Web.Authentication
{
    using System.Data.Entity;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Selise.Data.DatabaseContext;

    public static class AuthenticationSettings
    {
        public const string Provider = "Google";
        public const string ClientId = "579329085575-sophvd7i86vlfegp6qtqp2cbj1q20prd.apps.googleusercontent.com";
        public const string ClientSecret = "2q-1kDBbXywOy57IuOFRuIIK";

        public static async Task<bool> IsUserValid()
        {
            var db = new mySeliseEntities();
            var emailClaim = ClaimsPrincipal.Current.Claims.FirstOrDefault(c => c.Type.EndsWith("/emailaddress"));
            if (emailClaim == null) { return false; }

            var claimEmailAddress = emailClaim.Value;
            var employee = await db.Employees.FirstOrDefaultAsync(e => e.OfficialEmail == claimEmailAddress);
            return employee != null;
        }
    }
}