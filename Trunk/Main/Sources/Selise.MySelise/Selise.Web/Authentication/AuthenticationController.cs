﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;

namespace Selise.Web.Authentication
{
    using System.Data.Entity;
    using System.Runtime.CompilerServices;

    using Selise.Data.DatabaseContext;

    [Authorize]
    public class AuthenticationController : Controller
    {
        private IAuthenticationManager DefaultAuthenticationManager { get { return HttpContext.GetOwinContext().Authentication; } }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult ExternalLogin(string returnUrl)
        {
            return new ChallengeResult(AuthenticationSettings.Provider, Url.Action("ExternalLoginCallback", "Authentication", new { ReturnUrl = returnUrl }));
        }

        //[HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var isValidUserFound = await AuthenticationSettings.IsUserValid();
            if (isValidUserFound) { return RedirectToLocal(returnUrl); }
            DefaultAuthenticationManager.SignOut();
            return RedirectToAction("ExternalLoginFailure", "Authentication");
        }

        //[HttpPost]
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout(string returnUrl)
        {
            if (ModelState.IsValid)
            {
                DefaultAuthenticationManager.SignOut(AuthenticationSettings.Provider);
            }
            return RedirectToAction("Index", "Home");
        }

        #region Helper Functions
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult GoogleCallback(string returnUrl)
        {
            return new ViewResult();
        } 
        #endregion
    }

    public class ChallengeResult : HttpUnauthorizedResult
    {
        public string LoginProvider { get; set; }
        public string RedirectUri { get; set; }

        public ChallengeResult(string provider, string redirectUri)
        {
            LoginProvider = provider;
            RedirectUri = redirectUri;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.GetOwinContext().Authentication.Challenge(new AuthenticationProperties { RedirectUri = RedirectUri, IsPersistent = false }, LoginProvider);
        }
    }
}