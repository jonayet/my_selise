﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Google;
using Selise.Web.Authentication;

namespace Selise.Web
{
    using System.Security.Claims;
    using System.Web.Helpers;

    public partial class Startup
    {
        private void ConfigureAuth(IAppBuilder app)
        {
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Email;

            var cookieOptions = new CookieAuthenticationOptions
            {
                LoginPath = new PathString("/Authentication/ExternalLogin"),
                LogoutPath = new PathString("/Authentication/Logout"),
                ExpireTimeSpan = TimeSpan.FromMinutes(6 * 30 * 24 * 60),
                AuthenticationType = AuthenticationSettings.Provider,
            };

            app.UseCookieAuthentication(cookieOptions);
            app.SetDefaultSignInAsAuthenticationType(cookieOptions.AuthenticationType);

            app.UseGoogleAuthentication(
                new GoogleOAuth2AuthenticationOptions
                {
                    ClientId = AuthenticationSettings.ClientId,
                    ClientSecret = AuthenticationSettings.ClientSecret,
                    CallbackPath = new PathString("/Authentication/GoogleCallback"),
                }
            );
        }
    }
}
